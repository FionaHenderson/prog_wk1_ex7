﻿using System;

namespace Exercise7
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 23;
            var b = 30;
            var c = 7;
            var d = 15;
            var e = 5;
            var answer = (a + b) * c / (d - e);

            Console.WriteLine($"The answer is {answer}");

        }
    }
}
